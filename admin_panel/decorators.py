from django.shortcuts import HttpResponsePermanentRedirect, redirect
from django.contrib.sessions.models import Session
from django.urls import reverse

def verify_session(view_func):
 
        def wrapper_function(request, *args, **kwargs):

                sessions = request.session.get('username', False)
                print("SESSIONS",sessions)
                if sessions != False:
                    data = {}
                    data['id'] = sessions['Admin_Id']
                    data['name'] = sessions['Admin_Name']
                    data['roles'] = sessions['Admin_Roles']
                    data['token'] = sessions['Token']
                    kwargs['data'] = data
                    print("DEOCRATORS",">"*10, data['token'])
                    return view_func(request, *args, **kwargs)
                if not sessions:
                    return redirect('login')
      
        return wrapper_function
 

def clear_session(view_func):
    def wrapper_func(request, *args, **kwargs):
        sessions = request.session.get('username', False)
        if sessions:
            return redirect('logout')
        else:
            return view_func(request, *args, **kwargs)
    return wrapper_func



