import json

import requests
from django.http import HttpResponse, JsonResponse
from django.shortcuts import HttpResponse, redirect, render

from requests.models import Response
from .decorators import verify_session, clear_session
from django.contrib import messages
import json



# Create your views here.
def status_ok_ajax(response_data, kind=None):
    if kind == None or kind == 'json':
        res = JsonResponse(response_data.json())
        res.reason_phrase = json.dumps(response_data.json())
        res.status_code = response_data.status_code         
        return res
    else:
        res = JsonResponse(response_data.json())
        print("IN AJAX", response_data.json()['errors'][0])
        # print("length", len(json.loads(response_data.json()['errors'][0]['message'])))
        res.reason_phrase = json.dumps({'error':response_data.json()['errors'][0]['message']})
        res.status_code = response_data.status_code
        return res


@clear_session
def login(request):
    try:
        if request.method == 'POST':
            url = 'http://3.135.217.248:2567/api/admin/adminlogin'
            response_data = requests.put(url=url, json={"admin_name":request.POST['username'],"admin_password":request.POST['password']})
            print(response_data.status_code)
            print(response_data.json())
            if response_data.status_code == 200:       
                print("Response Data",response_data.json())
                request.session.create()
                request.session['username'] = response_data.json()['data'][0]
                res = redirect('home')
                res.set_cookie('user', request.session.session_key, samesite='lax', secure=True, httponly=True)
                # request.session.set_expiry('300*9')
                
                return res

            if response_data.status_code == 400:
                messages.error(request, 'Invalid Credentials')
                return redirect('login')

            if response_data.status_code == 403:
                messages.error(request,"YOU ARE BANNED ADMIN!!!")
                return redirect('login')

            if response_data.status_code == 401:
                messages.error(request,"YOU ARE BANNED ADMIN!!!")
                return redirect('login')
            

        return render(request, 'admin_panel/Login.html')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')
    except Exception as e:
        return HttpResponse(f"<h3 class='text-center'>Internal Server Error<h3> {e}")


@verify_session
def home(request, *args, **kwargs):
    try:
        id, name, roles, token = kwargs['data'].values()
        print("IN DASHBOARD VIEW FUNC")
        headers = {'Authorization': f'Bearer {token}'}
        response_data = requests.get('http://3.135.217.248:2567/api/admin/dashboard', headers=headers)
        
        if response_data.status_code == 200:
            print("RESP DATAS",response_data.json()['data'])
            context = {
                'admin' : name,
                'role' : roles,

            }
            context.update(response_data.json()['data'][0])
            res =  render(request, 'admin_panel/index.html', context)
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res
        if response_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif response_data.status_code == 401 or response_data.status_code == 400 :
            return redirect('login')
        else:
        
            return JsonResponse(response_data)
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')
    # except Exception as e:
    #     return HttpResponse(e.with_traceback)


@verify_session
def players(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        response_data = requests.get('http://3.135.217.248:2567/api/admin/userswallet', headers=headers)
        print(response_data.json())
        print(response_data.status_code)
        if response_data.status_code == 200:
            array = response_data.json()

            res = render(request, 'admin_panel/Players.html',context={'data':array})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

        if response_data.status_code == 401 or response_data.status_code == 400:
            messages.warning(request, "You need to Login Again")
            return redirect('login')

        if response_data.status_code == 404:
            return HttpResponse('Current Path not Found')

        if response_data.status_code == 403:
                messages.error(request, "You Are Banned Admin!!!")
                return redirect('login')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')
   
    
    
    # context = {data:data['data']}

@verify_session
def privillaged_players(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        url = 'http://3.135.217.248:2567/api/admin/userpreviledgealldetails'
        if request.method == "POST":
            user_id, priviladge = json.loads(request.body).values()
            url = 'http://3.135.217.248:2567/api/admin/updatepriviledgeuser'
            response_data = requests.put(url, json={"user_id":user_id, "previledge":priviladge},headers=headers)
            if response_data.status_code == 200:
                return status_ok_ajax(response_data)
            else:
                return status_ok_ajax(response_data, kind="error")
        response_data = requests.get(url, headers=headers)
        print(response_data.json())
        if response_data.status_code == 200:
            print(response_data.json())
            res = render(request, 'admin_panel/Privillageduser.html', context={'stars':response_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res
        if response_data.status_code != 500:
            messages.error(request, 'There was an error in game server(Internal Server Error')
            return render(request, 'admin_panel/Privillageduser.html')
        # if response_data.status_code == 401:
        #     return redirect('logout')
    except requests.ConnectionError as ce:
        return HttpResponse('game server is not up and running')

@verify_session
def banned_players(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        response_data = requests.get('http://3.135.217.248:2567/api/admin/bannedusers', headers=headers)
        if response_data.status_code == 200:
            res = render(request, 'admin_panel/Players.html', context={'banned':response_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

        if response_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif response_data.status_code == 401 or response_data.status_code == 400:
            return redirect('login')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')
    except Exception as e:
        return HttpResponse(e)


@verify_session
def banUser(request, *args, **kwargs):
    try:

        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if (request.method == "POST"):
            id = json.loads(request.body)['user_id']
            
            print(id)
            print("In Ban")
            response_data = requests.put("http://3.135.217.248:2567/api/admin/updateactiveuser", json={'user_id':id, 'is_active':False}, headers=headers)
            if response_data.status_code == 201 or response_data.status_code == 200:
                return status_ok_ajax(response_data)
            if response_data.status_code == 409:
                r = JsonResponse({'error': response_data.json()})
                r.status_code = 409
                return r
            print(response_data.status_code)
            return HttpResponse(response_data.json())
    
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

    except Exception as e:
        return HttpResponse(e)


@verify_session
def activate_player(request, *args, **kwargs):
    try: 
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if (request.method == "POST"):
            id, active = json.loads(request.body).values()
            
            print(id)
            print("In Ban")
            response_data = requests.put("http://3.135.217.248:2567/api/admin/updateactiveuser", json={'user_id':id, 'is_active':active}, headers=headers)
            if response_data.status_code == 201 or response_data.status_code == 200:
                return status_ok_ajax(response_data)
            if response_data.status_code == 409:
                r = JsonResponse({'error': response_data.json()})
                r.status_code = 409
                return r
            print(response_data.status_code)
            return HttpResponse(response_data.json())
    
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

    except Exception as e:
        return HttpResponse(e)


@verify_session
def player_by_id(request, pid, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        url = 'http://3.135.217.248:2567/api/admin/usersdetails'
        res_data = requests.post(url=url, json={'user_id':pid}, headers=headers)
        if res_data.status_code == 200:
            print(res_data.json())
            res = render(request, 'admin_panel/Players.html', context={'singles':res_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

        if res_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif res_data.status_code == 401 or res_data.status_code == 400:
            return redirect('login')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')
    except Exception as e:
        return HttpResponse(e)

@verify_session
def lobbies(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        url = 'http://3.135.217.248:2567/api/admin/alllobbies'
        response_data = requests.get(url=url, headers=headers)
        data = None
        if response_data.status_code == 200:
            data = response_data.json()
            print(data)
            res = render(request, 'admin_panel/Lobbies.html', context={'lobbies':data})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

        if response_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif response_data.status_code == 401 or response_data.status_code == 400:
            return redirect('login')
    
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

    except Exception as e:
        return HttpResponse(e)
    
    except Response.text as e:
        return HttpResponse("NOPE")


@verify_session
def userLobbyHistory(request, pid, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        url = 'http://3.135.217.248:2567/api/admin/searchuserlobbyhistorydata'
        res_data = requests.post(url=url, json={'user_id':pid},headers=headers)
        if res_data.status_code == 200:
            print(res_data.json())
            res = render(request, 'admin_panel/LobbiesHistory.html', context={'lobbies':res_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

        if res_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif res_data.status_code == 401 or res_data.status_code == 401:
            return redirect('login')
        elif res_data.status_code == 403:
            messages.error(request, "You Are Banned Admin!!!")
            return redirect('login')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')
    except Exception as e:
        return HttpResponse(e)


@verify_session
def lobbyHistory(request, lid, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        url = 'http://3.135.217.248:2567/api/admin/searchlobbyhistorydata'
        res_data = requests.post(url=url, json={'lobby_id':lid},headers=headers)
        if res_data.status_code == 200:
            print(res_data.json())
            res = render(request, 'admin_panel/LobbiesHistory.html', context={'lobbies':res_data.json(), 'lb_hists':True})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res
            
        if res_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif res_data.status_code == 401 or res_data.status_code == 401:
            return redirect('login')
        elif res_data.status_code == 403:
            messages.error(request, "You Are Banned Admin!!!")
            return redirect('login')

            
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')


@verify_session
def lb_structure(request, aid,*args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            if 'all' in request.POST:
                url = 'http://3.135.217.248:2567/api/admin/alllobbystructure'
                response_data = requests.post(url=url, json={"admin_id":aid}, headers=headers)
                if response_data.status_code == 200:
                        all = response_data.json()['All']
                        print("ALL LOBBIES STRUCT", all)
                        res = render(request, 'admin_panel/LobbyStructures.html', context={'all':all})
                        res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
                        res["Pragma"] = "no-cache" # HTTP 1.0.
                        res["Expires"] = "0" # Proxies.
                        return res
                if response_data.status_code == 500:

                    return HttpResponse('Internal Server Error')
                elif response_data.status_code == 401 or response_data.status_code == 400:
                    return redirect('login')
                else:
                    print(response_data.json())
                    return HttpResponse(f'{response_data.json()}')

            url = 'http://3.135.217.248:2567/api/admin/createlobbystructure'
            data = json.loads(request.body)
            name, fee, cut, player, id = list(data.values())
            res_data = requests.post(url=url, json={"ls_name":name, "ls_entry_fees":fee, "ls_per_cut":cut, "ls_players":player, "admin_id":id}, headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)
            else:
                return status_ok_ajax(res_data, kind="erros")
        url = 'http://3.135.217.248:2567/api/admin/alllobbystructure'
        response_data = requests.post(url=url, json={"admin_id":aid}, headers=headers)
        if response_data.status_code == 200:
            admin = response_data.json()['Admin']
            print("ADMINS LOBBY", admin)
            res = render(request, 'admin_panel/LobbyStructures.html', context={'structs':admin})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res
        if response_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif response_data.status_code == 401 or response_data.status_code == 401:
            return redirect('login')
        elif response_data.status_code == 403:
            messages.error(request, "You Are Banned Admin!!!")
            return redirect('login')


    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

    except Exception as e:
        return HttpResponse(e)


@verify_session
def update_lb_structures(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            url = 'http://3.135.217.248:2567/api/admin/updatelobbystructure'
            ls_id, ls_name, ls_entry_fees, ls_per_cut, ls_players = json.loads(request.body).values()
            print(json.loads(request.body).values())
            res_data = requests.put(url=url, json={"ls_id":ls_id, "ls_name":ls_name, "ls_entry_fees":ls_entry_fees, "ls_per_cut":ls_per_cut, "ls_players":ls_players}, headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)
            else:
                return status_ok_ajax(res_data,kind='errors')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

    except Exception as e:
        return HttpResponse(e)


@verify_session
def delete_lb_structures(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            url = 'http://3.135.217.248:2567/api/admin/deletelobbystructure'
            ls_id = json.loads(request.body)['ls_id']
            print(json.loads(request.body).values())
            res_data = requests.delete(url=url, json={"ls_id":ls_id}, headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)
            else:
                return status_ok_ajax(res_data,kind='errors')
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

    except Exception as e:
        return HttpResponse(e)

@verify_session
def update_active_structures(request, aid=None, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            ls_id, active = json.loads(request.body).values()
            print(ls_id, active)
            url = 'http://3.135.217.248:2567/api/admin/updateactivelobbystructure'
            response_data = requests.put(url=url, json={"ls_id":ls_id, "is_active":active}, headers=headers)
            if response_data.status_code == 200:
                return status_ok_ajax(response_data)
            else:
                return status_ok_ajax(response_data,kind='errors')


    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

@verify_session
def all_banned_lbs(request, aid=None, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            url = 'http://3.135.217.248:2567/api/admin/bannedalllobbystructure'
            response_data = requests.post(url=url, json={"admin_id":aid}, headers=headers)
            if response_data.status_code == 200:
                all = response_data.json()['All']
                res = render(request, 'admin_panel/LobbyStructureActive.html', context={'all_adlbs':all})
                res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
                res["Pragma"] = "no-cache" # HTTP 1.0.
                res["Expires"] = "0" # Proxies.
                return res
            if response_data.status_code == 500:
                return HttpResponse('Internal Server Error')
            elif response_data.status_code == 401 or response_data.status_code == 400:
                return redirect('login')
            elif response_data.status_code == 403:
                messages.error(request, "You Are Banned Admin!!!")
                return redirect('login')

        url = 'http://3.135.217.248:2567/api/admin/bannedalllobbystructure'
        response_data = requests.post(url=url, json={"admin_id":aid}, headers=headers)
        if response_data.status_code == 200:
            admin = response_data.json()['Admin']
            res = render(request, 'admin_panel/LobbyStructureActive.html', context={'adlbs':admin})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res
        if response_data.status_code == 500:
                return HttpResponse('Internal Server Error')
        elif response_data.status_code == 401 or response_data.status_code == 400:
                return redirect('login')
        elif response_data.status_code == 403:
                messages.error(request, "You Are Banned Admin!!!")
                return redirect('login')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')


@verify_session
def adminData(request,*args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
       
        if (request.method == 'POST'):
            name, role, password = json.loads(request.body).values()
            print("In POST")
            response_data = requests.post("http://3.135.217.248:2567/api/admin/createadmin", json={'admin_name':name, 'admin_role':role, 'admin_password':password}, headers=headers)
            if response_data.status_code == 201 or response_data.status_code == 200:
                return status_ok_ajax(response_data)
            if response_data.status_code != 200: 
                return status_ok_ajax(response_data,kind='errors')

            return redirect('admins')
        # else: raise InvalidProxyURL()
        
       

        response_data = requests.get(url='http://3.135.217.248:2567/api/admin/admindata', headers=headers)
        print(response_data.status_code)
        if response_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        if response_data.status_code == 401 or response_data.status_code == 400:
            return redirect('login')
        if response_data.status_code == 403:
                messages.error(request, "You Are Banned Admin!!!")
                return redirect('login')

        if response_data.status_code == 200:
            print(response_data.json())
            admins = response_data.json()
            res = render(request, 'admin_panel/Admin.html', context={'admin_data':admins})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

    except Exception as e:
        return HttpResponse(e)


@verify_session
def modifyAdmin(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        print("Modify Called")
        if (request.method == "POST"):
            id, name, role, password, isactive = json.loads(request.body).values()
            print(json.loads(request.body).values())
            print("In PUT")
            response_data = requests.put("http://3.135.217.248:2567/api/admin/updateadmindata", json={'admin_id':id, 'admin_name':name, 'admin_role':role, 'admin_password':password,"is_active":isactive}, headers=headers)
            if response_data.status_code == 201 or response_data.status_code == 200:
                return status_ok_ajax(response_data)
            else:
                return status_ok_ajax(response_data, kind="errors")

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')


@verify_session
def deleteAdmin(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        print("DELETE Called")
        print(json.loads(request.body))
        if (request.method == "POST"):
            
            id = json.loads(request.body)['admin_id']
            print(id)
            print("In DELETE")
            response_data = requests.delete("http://3.135.217.248:2567/api/admin/deleteadmin", json={'admin_id':id}, headers=headers)
            if response_data.status_code == 201 or response_data.status_code == 200:
                return status_ok_ajax(response_data)
            else:
                return status_ok_ajax(response_data, kind="errors")

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')


@verify_session
def banned_admins(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            print("IN POSTS")
            url = "http://3.135.217.248:2567/api/admin/updatebannedadmindata"
            id, active = json.loads(request.body).values()
            print(id , active)
            response_data = requests.put(url=url, json={"admin_id":id,"is_active":active},headers=headers)
            if response_data.status_code == 200:
                return status_ok_ajax(response_data)
            else:
                return status_ok_ajax(response_data, kind="errors")

        response_data = requests.get("http://3.135.217.248:2567/api/admin/bannedadmindata", headers=headers)
        if response_data.status_code == 200:
            print(response_data.json())
            res = render(request, 'admin_panel/Admin.html', context={'banned':response_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res
        else:
            print(response_data.json())
            print(response_data.status_code)
            return HttpResponse(response_data.json())

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

@verify_session
def commissions(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        url = 'http://3.135.217.248:2567/api/admin/alllobbiescommision'
        resp_data = requests.get(url=url, headers=headers)
        if resp_data.status_code == 200:
            data = resp_data.json()['data1']
            total = resp_data.json()['total']
            context = {
                'cmns_data':data,
                'totals':total,
                'pressed': json.dumps(data)
            }
            print(context)
        if resp_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif resp_data.status_code == 401 or resp_data.status_code == 400:
            return redirect('login')
        if resp_data.status_code == 403:
                messages.error(request, "You Are Banned Admin!!!")
                return redirect('login')
        res = render(request, 'admin_panel/Commission.html', context)
        res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
        res["Pragma"] = "no-cache" # HTTP 1.0.
        res["Expires"] = "0" # Proxies.
        return res

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')


@verify_session
def achievements(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            title, statement, count, reward = json.loads(request.body).values()
            url = 'http://3.135.217.248:2567/api/admin/createachievements'
            res_data = requests.post(url=url, json={"title":title, "statement":statement, "total_count":count, "reward":reward}, headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)
            else:
                return status_ok_ajax(res_data, kind="errors")
        url = 'http://3.135.217.248:2567/api/admin/allachievements'
        resp_data = requests.get(url=url, headers=headers)
        if resp_data.status_code == 200:
            res = render(request, 'admin_panel/achievement.html', context={'data':resp_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res
        if resp_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif resp_data.status_code == 401 or resp_data.status_code == 400:
            return redirect('login')
        elif resp_data.status_code == 403:
                messages.error(request, "You Are Banned Admin!!!")
                return redirect('login')
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')


@verify_session
def updateAch(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            print("In Update Achievement")
            id, title, statement, count, reward = json.loads(request.body).values()
            url = 'http://3.135.217.248:2567/api/admin/updateachievementsData'
            res_data = requests.put(url=url, json={"achievements_id":id,"title":title, "statement":statement, "total_count":count, "reward":reward}, headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)
            else:
                return status_ok_ajax(res_data, kind="errors")
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')


@verify_session
def deleteAchievement(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            print("In Update Achievement")
            id = json.loads(request.body)['achievements_id']
            url = 'http://3.135.217.248:2567/api/admin/deleteachievements'
            res_data = requests.delete(url=url, json={"achievements_id":id}, headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)
            else:
                return status_ok_ajax(res_data, kind="erros")
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')


@verify_session
def variations(request, *args, **kwargs):
    try:

        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            res_data = None
            if 'variation_id' in json.loads(request.body).keys():
                id, name, initial, count = json.loads(request.body).values()
                url = 'http://3.135.217.248:2567/api/admin/updatevariationsData'
                res_data = requests.put(url=url, json={"variation_id":id,"variation_name":name,"variation_initial":initial,"player_count":count}, headers=headers)
                
                    
            else:
                name, initial, count = json.loads(request.body).values()
                url = 'http://3.135.217.248:2567/api/admin/createvariation'
                res_data = requests.post(url=url, json={"variation_name":name,"variation_initial":initial,"player_count":count}, headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)
            else:
                return status_ok_ajax(res_data, kind="errors")
        url = 'http://3.135.217.248:2567/api/admin/variations'
        res_data = requests.get(url=url, headers=headers)
        if res_data.status_code == 200:
            # print(res_data.json())
            res = render(request, 'admin_panel/Variations.html', context={'data':res_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

        if res_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif res_data.status_code == 401 or res_data.status_code == 401:
            return redirect('login')
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

@verify_session
def delete_variation(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            print("In delete variation")
            id = json.loads(request.body)['variation_id']
            url = 'http://3.135.217.248:2567/api/admin/deletevariation'
            res_data = requests.delete(url=url, json={"variation_id":id}, headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)
            else:
                return status_ok_ajax(res_data, kind="erros")
        else:
            return HttpResponse('Invalid Requests')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

@verify_session
def leisure(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        if request.method == 'POST':
            lbid = json.loads(request.body)['user_id']
            url = 'http://3.135.217.248:2567/api/admin/userleisuredata'
            res_data = requests.post(url=url, json={'user_id':lbid},headers=headers)
            if res_data.status_code == 200:
                return status_ok_ajax(res_data)

        url = 'http://3.135.217.248:2567/api/admin/leisurealldata'
        res_data = requests.get(url=url, headers=headers)
        print("LEISURE VIEWS",res_data.json())
        if res_data.status_code == 200:
            print(res_data.json())
            res = render(request, 'admin_panel/Leisure.html', context={'allleisures':res_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res
        if res_data.status_code == 401 or res_data.status_code == 400:
            return redirect('login')
        if res_data.status_code == 500:
            messages.warning(request, 'There is an Internal Server error')
            res = render(request,'admin_panel/Leisure.html')
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')
    # except Exception as e:
    #     print(e)
    #     messages.warning(request, "You have been logged out")
    #     return HttpResponse(e)

@verify_session
def leisurePlayer(request, lid,*args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        url = 'http://3.135.217.248:2567/api/admin/leisuredata'
        res_data = requests.post(url=url, json={'lobby_id':lid}, headers=headers) 
        if res_data.status_code == 200:
            res = render(request, 'admin_panel/LeisurePlayer.html', context={'lps':res_data.json()})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

        if res_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        elif res_data.status_code == 401 or res_data.status_code == 400:
            return redirect('login')

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')

@verify_session
def withdrawals(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        context =  None
        if request.method == 'POST':
            uid = json.loads(request.body)['user_id']
            url = 'http://3.135.217.248:2567/api/admin/userpayments'
            res_data = requests.post(url=url, json={'user_id':uid}, headers=headers)
            print(res_data.json())
            a = {'data':res_data.json()}
            res = JsonResponse(a)
            res.status_code = 200
            return res

        url='http://3.135.217.248:2567/api/admin/allpayments'
        res_data = requests.get(url=url, headers=headers)
        if res_data.status_code == 200:
            print(res_data.json())
            res = render(request, 'admin_panel/Withdrawals.html', context={'payments':res_data.json()['data1'], 'totals': res_data.json()['total1']})
            res["Cache-Control"] = "no-cache, no-store, must-revalidate" # HTTP 1.1.
            res["Pragma"] = "no-cache" # HTTP 1.0.
            res["Expires"] = "0" # Proxies.
            return res

        if res_data.status_code == 500:
            return HttpResponse('Internal Server Error')
        if res_data.status_code == 401 or res_data.status_code == 400:
            return redirect('login')
        if res_data.status_code == 403:
                messages.error(request, "You Are Banned Admin!!!")
                return redirect('login')
    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2></h2></h2>')

@verify_session
def logut(request, *args, **kwargs):
    try:
        token = kwargs['data']['token']
        headers = {'Authorization': f'Bearer {token}'}
        request.session.clear()
        return redirect('login')
    except Exception as e:
        print(e)


@verify_session
def create_new_session(request, *args, **kwargs):
    try:
        id, name, role, token = kwargs['data'].values()
        request.session.clear()
        print("IN Regenerate")
        url = 'http://3.135.217.248:2567/api/admin/regeneratetoken'
        headers = {'Authorization':f'Bearer {token}'}
        response_data = requests.put(url=url, json={'Admin_Id':id,'Admin_Name':name,'Admin_Roles':role}, headers=headers)
        print(response_data.status_code)
        if response_data.status_code == 200:
            response_d = response_data.json()[0]
            print("regenerated", "="*10, response_d['Token'])
            request.session.create()
            request.session['username'] = response_d
            request.session.modified = True        
            res = HttpResponse('OK')
            res.set_cookie('user', request.session.session_key)
            return res
        if response_data.status_code == 401 or response_data.status_code == 400:
            print(response_data.json())
            return redirect('login')
        print(response_data.status_code)
        print(response_data.json())

    except requests.ConnectionError as err:
        return HttpResponse('<h2>Server is not up and running</h2>')
    except Exception as e:
        return HttpResponse(e)

